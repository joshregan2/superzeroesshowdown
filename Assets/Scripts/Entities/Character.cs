using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
	public int PlayerId {get; set;}
	public CharacterStats Stats {get; set;}
	public int CurrentHealth {get; set;}
	public PowersConfig.PowerTypes EquippedPower {get; set;}

	public CharacterConfig.CharacterStates CurrentState {get; set;}
	private int respawnCounter;

	private SpriteRenderer spriteRenderer;
	private CircleCollider2D circleCollider;

	public Vector2 CurrentHeading {get; set;}

	void Awake()
	{
		spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
		CurrentHeading = new Vector2(0, 0);
		respawnCounter = 0;
		CurrentState = CharacterConfig.CharacterStates.ACTIVE;
	}

	// Use this for initialization
	void Start()
	{
		Rigidbody2D body = gameObject.AddComponent<Rigidbody2D>();
		body.gravityScale = 0;
		body.drag = Stats.DragAmount;
		body.fixedAngle = true;

		circleCollider = gameObject.AddComponent<CircleCollider2D>();
		circleCollider.center = new Vector2(0,-(spriteRenderer.sprite.bounds.size.x * 0.25f)/2.0f);
		circleCollider.radius = (spriteRenderer.sprite.bounds.size.x * 0.9f)/2.0f;
	}

	// Update is called once per frame
	void Update()
	{
		if (CurrentState == CharacterConfig.CharacterStates.RESPAWNING)
		{
			if (respawnCounter < GlobalConfig.CHARACTER_RESPAWN_TIMER)
			{
				respawnCounter++;
			}
			else
			{
				respawnCounter = 0;
				Vector2 nextAvailableSpawn = GameManager.SharedGameManager.EnvironmentManager.NextAvailablePlayerSpawnLocation();
				gameObject.transform.position = new Vector3(nextAvailableSpawn.x, nextAvailableSpawn.y, 0);
				ShowCharacter();
				CurrentState = CharacterConfig.CharacterStates.ACTIVE;
			}
		}
	}

	public void ConfigureForCharacter(CharacterConfig.CharacterTypes characterType)
	{
		EquippedPower = PowersConfig.PowerTypes.EMPTY;
		Stats = CharacterConfig.CharacterStatsDefinitions[characterType].Clone();
		CurrentHealth = Stats.MaxHealth;

		spriteRenderer.sprite = Resources.Load<Sprite>(Stats.SpriteFile);
		spriteRenderer.sortingOrder = 5;
	}

	private void MoveWithVelocity(Vector2 velocity)
	{
		gameObject.rigidbody2D.AddForce(velocity*Stats.MoveSpeed);
	}

	public void MoveLeft(float force)
	{
		MoveWithVelocity(new Vector2(force,0));
	}

	public void MoveRight(float force)
	{
		MoveWithVelocity(new Vector2(force,0));
	}

	public void MoveUp(float force)
	{
		MoveWithVelocity(new Vector2(0,force));
	}

	public void MoveDown(float force)
	{
		MoveWithVelocity(new Vector2(0,force));
	}

	public void UsePower()
	{
		// Action depends on currently equipped power
		GameManager.SharedGameManager.PowerManager.ExecutePower(this, EquippedPower);

		EquippedPower = PowersConfig.PowerTypes.EMPTY;
		GameManager.SharedGameManager.GUIManager.GetPlayerHUDForPlayerId(PlayerId).SetPower(EquippedPower);
	}

	public void EquipPower(PowersConfig.PowerTypes powerType)
	{
		EquippedPower = powerType;

		// Update HUD
		GameManager.SharedGameManager.GUIManager.GetPlayerHUDForPlayerId(PlayerId).SetPower(EquippedPower);
	}

	public void DamageCharacter(int damage, int fromPlayerId)
	{
		TweenColor.Begin(gameObject, 0.2f, Color.red).AddOnFinished(new EventDelegate(DamageFlashComplete));

		CurrentHealth = Mathf.Max(CurrentHealth - damage, 0);
		if (CurrentHealth == 0)
		{
			CharacterDefeated(fromPlayerId);
		}

		GameManager.SharedGameManager.GUIManager.GetPlayerHUDForPlayerId(PlayerId).SetHealth(CurrentHealth, Stats.MaxHealth);

	}

	private void DamageFlashComplete()
	{
		TweenColor.current.RemoveOnFinished(new EventDelegate(DamageFlashComplete));
		TweenColor.Begin(gameObject, 0.2f, Color.white);
	}

	private void CharacterDefeated(int byPlayerId)
	{
		// TODO: Defeat animation would run here
		GameManager.SharedGameManager.CurrentGameSession.AddPointToScoreForPlayer(byPlayerId);
		HideCharacter();
		CurrentState = CharacterConfig.CharacterStates.RESPAWNING;
	}

	private void HideCharacter()
	{
		gameObject.renderer.enabled = false;
		Renderer[] childRenderers = gameObject.GetComponentsInChildren<Renderer>();
		foreach (Renderer childRenderer in childRenderers)
		{
			childRenderer.enabled = false;
		}
	}

	private void ShowCharacter()
	{
		gameObject.renderer.enabled = true;
		Renderer[] childRenderers = gameObject.GetComponentsInChildren<Renderer>();
		foreach (Renderer childRenderer in childRenderers)
		{
			childRenderer.enabled = true;
		}
	}
}

