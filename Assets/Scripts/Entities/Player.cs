using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using InControl;

public class Player : MonoBehaviour
{
	public int PlayerId {get; set;}

	private Character Character {get; set;}

	void Awake()
	{

	}

	// Use this for initialization
	void Start()
	{
		Character = gameObject.GetComponent<Character>();
	}

	// Update is called once per frame
	void Update()
	{
		// Check that the player is active and not respawning
		if (Character.CurrentState == CharacterConfig.CharacterStates.ACTIVE &&
		    GameManager.SharedGameManager.CurrentGameSession.ActivePlayers[PlayerId].IsComputer == false)
		{
			// For local multiplayer on the same machine with controllers
			if (GameManager.SharedGameManager.CurrentGameSession.GameMode == GlobalConfig.GameModes.SINGLEPLAYER ||
				GameManager.SharedGameManager.CurrentGameSession.GameMode == GlobalConfig.GameModes.LOCAL_MULTIPLAYER)
			{
				foreach (KeyValuePair<int, PlayerStats> kvp in GameManager.SharedGameManager.CurrentGameSession.ActivePlayers)
				{
					if (kvp.Value.IsActive == true && kvp.Value.IsComputer == false && kvp.Key == PlayerId)
					{
						float horizontalAxis = InputManager.Devices[PlayerId-1].LeftStickX.Value;
						if (horizontalAxis < 0)
						{
							Character.MoveLeft(horizontalAxis);
						}
						else if (horizontalAxis > 0)
						{
							Character.MoveRight(horizontalAxis);
						}

						float verticalAxis = InputManager.Devices[PlayerId-1].LeftStickY.Value;
						if (verticalAxis < 0)
						{
							Character.MoveUp(verticalAxis);
						}
						else if (verticalAxis > 0)
						{
							Character.MoveDown(verticalAxis);
						}

						Character.CurrentHeading = new Vector2(horizontalAxis, verticalAxis);

						if (InputManager.Devices[PlayerId-1].Action1.WasPressed == true)
						{
							Character.UsePower();
						}

						break;
					}
				}
			}
			else 
			{
				float horizontalAxis = 1;
				if (Input.GetKey(KeyCode.A))
				{
					Character.MoveLeft(-horizontalAxis);
				}
				else if (Input.GetKey(KeyCode.D))
				{
					Character.MoveRight(horizontalAxis);
				}
				
				float verticalAxis = 1;
				if (Input.GetKey(KeyCode.W))
				{
					Character.MoveUp(verticalAxis);
				}
				else if (Input.GetKey(KeyCode.S))
				{
					Character.MoveDown(-verticalAxis);
				}

				Character.CurrentHeading = new Vector2(horizontalAxis, verticalAxis);
				
				if (Input.GetKeyDown(KeyCode.Space))
				{
					Character.UsePower();
				}
			}
		}
	}
}

