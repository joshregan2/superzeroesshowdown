using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour
{
	private SpriteRenderer spriteRenderer;
	private PowersConfig.PowerTypes powerType;
	
	void Awake ()
	{
		spriteRenderer = gameObject.AddComponent<SpriteRenderer> ();
	}

	// Use this for initialization
	void Start ()
	{
		CircleCollider2D circleCollider = gameObject.AddComponent<CircleCollider2D> ();
		circleCollider.isTrigger = true;
	}

	// Update is called once per frame
	void Update ()
	{

	}

	void OnTriggerEnter2D(Collider2D other) {
		Character character = other.gameObject.GetComponent<Character>();
		if (character != null)
		{
			character.EquipPower(powerType);

			GameManager.SharedGameManager.EnvironmentManager.AllCollectibles.Remove(this);
			GameObject.Destroy(gameObject);
		}
	}

	public void ConfigureForPower(PowersConfig.PowerTypes powerType)
	{
		this.powerType = powerType;
		spriteRenderer.sprite = Resources.Load<Sprite>("UI/player-marker");
		spriteRenderer.sortingOrder = 6;
	}
}

