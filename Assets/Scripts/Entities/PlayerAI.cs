using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAI : MonoBehaviour
{
	public const float AI_LONG_POWER_ATTACK_RANGE = 2.0f;
	public const float AI_MID_POWER_ATTACK_RANGE = 1.5f;
	public const float AI_SHORT_POWER_ATTACK_RANGE = 1.0f;

	public int PlayerId {get; set;}
	private Character Character {get; set;}

	private static readonly List<Vector3> possibleMoves = new List<Vector3>()
	{
		new Vector3(0, 1.0f, 0),
		new Vector3(1.0f, 1.0f, 0),
		new Vector3(1.0f, 0, 0),
		new Vector3(1.0f, -1.0f, 0),
		new Vector3(0, -1.0f, 0),
		new Vector3(-1.0f, -1.0f, 0),
		new Vector3(-1.0f, 0, 0),
		new Vector3(-1.0f, 1.0f, 0)
	};

	// Use this for initialization
	void Start ()
	{
		Character = gameObject.GetComponent<Character>();
	}

	// Update is called once per frame
	void Update ()
	{
		MakeNextDecision();
	}

	private void MakeNextDecision()
	{
		if (CheckIfPlayerShouldFindPower() == true)
		{
			MoveTowardsLocation(GetPositionOfNearestCollectible());
		}
		else if (CheckIfPlayerShouldAttack() == true)
		{
			UsePowerOnNearestCharacter();
		}
		else
		{
//			MoveInDirection(SelectRandomMoveDirection());
		}
	}

	private bool CheckIfPlayerShouldFindPower()
	{
		if (Character.EquippedPower == PowersConfig.PowerTypes.EMPTY)
		{
			return true;
		}

		return false;
	}

	private bool CheckIfPlayerShouldAttack()
	{
		if (Character.EquippedPower != PowersConfig.PowerTypes.EMPTY)
		{
			return true;
		}

		return false;
	}

	private bool HasLongRangePowerEquipped()
	{
		// TODO: Will check against powers for range
		return false;
	}

	private bool HasMidRangePowerEquipped()
	{
		// TODO: Will check against powers for range
		switch(Character.EquippedPower)
		{
			case PowersConfig.PowerTypes.FIREBALL:
				return true;
			default:
				return false;
		}
	}

	private void UsePowerOnNearestCharacter()
	{
		Vector3 nearestCharacterPosition = GetPositionOfNearestCharacter();
		float distance = Vector3.Distance(gameObject.transform.position, nearestCharacterPosition);
		Vector3 direction = nearestCharacterPosition - gameObject.transform.position;
		direction.Normalize();
		
		if (HasLongRangePowerEquipped() == true)
		{
			if (distance <= AI_LONG_POWER_ATTACK_RANGE)
			{
				MoveTowardsLocation(nearestCharacterPosition);
				Character.CurrentHeading = new Vector2(direction.x , direction.y);
				Character.UsePower();
			}
			else
			{
				MoveTowardsLocation(nearestCharacterPosition);
			}
		}
		else if (HasMidRangePowerEquipped() == true)
		{
			if (distance <= AI_MID_POWER_ATTACK_RANGE)
			{
				MoveTowardsLocation(nearestCharacterPosition);
				Character.CurrentHeading = new Vector2(direction.x , direction.y);
				Character.UsePower();
			}
			else
			{
				MoveTowardsLocation(nearestCharacterPosition);
			}
		}
		else
		{
			if (distance <= AI_SHORT_POWER_ATTACK_RANGE)
			{
				MoveTowardsLocation(nearestCharacterPosition);
				Character.CurrentHeading = new Vector2(direction.x , direction.y);
				Character.UsePower();
			}
			else
			{
				MoveTowardsLocation(nearestCharacterPosition);
			}
		}
	}

	private Vector3 GetPositionOfNearestCollectible()
	{
		Vector3 nearest = Vector3.zero;
		float bestDistance = 0;
		for (int i = 0; i < GameManager.SharedGameManager.EnvironmentManager.AllCollectibles.Count; i++)
		{
			float distance = Vector3.Distance(gameObject.transform.position,
			                                  GameManager.SharedGameManager.EnvironmentManager.AllCollectibles[i].gameObject.transform.position);
			if (bestDistance == 0 || distance < bestDistance)
			{
				bestDistance = distance;
				nearest = GameManager.SharedGameManager.EnvironmentManager.AllCollectibles[i].gameObject.transform.position;
			}
		}

		return nearest;
	}

	private Vector3 GetPositionOfNearestCharacter()
	{
		Vector3 nearest = Vector3.zero;
		float bestDistance = 0;
		for (int i = 0; i < GameManager.SharedGameManager.EntityManager.AllCharacters.Count; i++)
		{
			float distance = Vector3.Distance(gameObject.transform.position,
			                                  GameManager.SharedGameManager.EntityManager.AllCharacters[i].gameObject.transform.position);
			if (bestDistance == 0 || distance < bestDistance)
			{
				bestDistance = distance;
				nearest = GameManager.SharedGameManager.EntityManager.AllCharacters[i].gameObject.transform.position;
			}
		}
		
		return nearest;
	}

	private void MoveTowardsLocation(Vector3 location)
	{
		Vector3 direction = location - gameObject.transform.position;
		direction.Normalize();

		MoveInDirection(direction);
	}

	private void MoveInDirection(Vector3 direction)
	{	
		if (direction.x < 0)
		{
			Character.MoveLeft(-1);
		}
		else if (direction.x > 0)
		{
			Character.MoveRight(1);
		}
		
		if (direction.y > 0)
		{
			Character.MoveUp(1);
		}
		else if (direction.y < 0)
		{
			Character.MoveDown(-1);
		}
	}

	private Vector3 SelectRandomMoveDirection()
	{
		return possibleMoves[Random.Range(0, possibleMoves.Count-1)];
	}

	private Vector3 GetBestMoveToTarget(Vector3 target)
	{
		float bestDistanceToTarget = 0;
		Vector3 bestMove = Vector3.zero;
		
		for (int i = 0; i < possibleMoves.Count; i++)
		{
			float distanceToTarget = Vector3.Distance(gameObject.transform.position + possibleMoves[i], target);
			
			if (bestDistanceToTarget == 0 || distanceToTarget < bestDistanceToTarget)
			{
				bestDistanceToTarget = distanceToTarget;
				bestMove = possibleMoves[i];
			}
		}
		
		return bestMove;
	}

	private Vector3 GetBestMoveAwayFromObstacle(Vector3 obstacle)
	{
		float bestDistanceAwayFromObstacle = 0;
		Vector3 bestMove = Vector3.zero;
		
		for (int i = 0; i < possibleMoves.Count; i++)
		{
			float distanceToObstacle = Vector3.Distance(gameObject.transform.position + possibleMoves[i], obstacle);
			
			if (bestDistanceAwayFromObstacle == 0 || distanceToObstacle > bestDistanceAwayFromObstacle)
			{
				bestDistanceAwayFromObstacle = distanceToObstacle;
				bestMove = possibleMoves[i];
			}
		}
		
		return bestMove;
	}

	private Vector3 GetBestMoveToTargetAwayFromObstacle(Vector3 target, Vector3 obstacle)
	{
		float bestDistanceToTarget = 0;
		float bestDistanceAwayFromObstacle = 0;
		Vector3 bestMove = Vector3.zero;

		for (int i = 0; i < possibleMoves.Count; i++)
		{
			float distanceToTarget = Vector3.Distance(gameObject.transform.position + possibleMoves[i], target);
			float distanceToObstacle = Vector3.Distance(gameObject.transform.position + possibleMoves[i], obstacle);
				
			if ((bestDistanceToTarget == 0 && bestDistanceAwayFromObstacle == 0) || 
			    (distanceToTarget < bestDistanceToTarget && distanceToObstacle > bestDistanceAwayFromObstacle))
			{
				bestDistanceToTarget = distanceToTarget;
				bestDistanceAwayFromObstacle = distanceToObstacle;
				bestMove = possibleMoves[i];
			}
		}

		return bestMove;
	}

	private void HandleCollisionForSituation(Collision2D collision)
	{
		if (CheckIfPlayerShouldFindPower() == true)
		{
			GetBestMoveToTargetAwayFromObstacle(GetPositionOfNearestCollectible(), collision.collider.gameObject.transform.position);
		}
		else if (CheckIfPlayerShouldAttack() == true)
		{
			GetBestMoveToTargetAwayFromObstacle(GetPositionOfNearestCharacter(), collision.collider.gameObject.transform.position);
		}
		else
		{
			GetBestMoveAwayFromObstacle(collision.collider.gameObject.transform.position);
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		HandleCollisionForSituation(collision);
	}

	void OnCollisionStay2D(Collision2D collision)
	{
		HandleCollisionForSituation(collision);
	}


}

