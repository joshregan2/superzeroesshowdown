using UnityEngine;
using System.Collections;

public class PowerSuperSpeed : MonoBehaviour
{
	public Character Wielder {get; set;}
	private ParticleSystem ghostTrailParticleSystem;
	private EventTimer timer;

	void Awake()
	{

	}

	// Use this for initialization
	void Start ()
	{
		ghostTrailParticleSystem = (ParticleSystem)GameObject.Instantiate(Resources.Load<ParticleSystem>("Powers/super-speed-trail-prefab"));
		ghostTrailParticleSystem.renderer.material.mainTexture = Wielder.gameObject.GetComponent<SpriteRenderer>().sprite.texture;
		ghostTrailParticleSystem.renderer.sortingOrder = 5;
		ghostTrailParticleSystem.gameObject.transform.position = gameObject.transform.position;
		ghostTrailParticleSystem.gameObject.transform.parent = gameObject.transform;

		timer = new EventTimer(false, new EventDelegate(PowerExpired));
		timer.StartTimer(PowersConfig.SUPER_SPEED_DURATION);

		Wielder.Stats.MoveSpeed *= 2;
	}

	// Update is called once per frame
	void Update ()
	{
		timer.Update();
	}

	private void PowerExpired()
	{
		Wielder.Stats.MoveSpeed *= 0.5f;
		GameObject.Destroy(gameObject);
	}
}

