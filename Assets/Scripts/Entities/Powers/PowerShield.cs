using UnityEngine;
using System.Collections;

public class PowerShield : MonoBehaviour
{
	public Character Wielder {get; set;}
	private SpriteRenderer spriteRenderer;
	private CircleCollider2D circleCollider;
	private EventTimer timer;

	void Awake()
	{

	}

	// Use this for initialization
	void Start ()
	{
		spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
		spriteRenderer.sprite = Resources.Load<Sprite>("Powers/shield");
		spriteRenderer.sortingOrder = 6;

		circleCollider = gameObject.AddComponent<CircleCollider2D>();
		circleCollider.radius = spriteRenderer.sprite.texture.width*0.6f;
		circleCollider.isTrigger = true;

		timer = new EventTimer(false, new EventDelegate(PowerExpired));
		timer.StartTimer(PowersConfig.SHIELD_DURATION);
	}

	// Update is called once per frame
	void Update ()
	{
		timer.Update();
	}

	void OnTriggerEnter2D(Collider2D collider) 
	{	
		bool didReflect = false;

		// Reflect projectiles
		PowerFireball fireball = collider.gameObject.GetComponent<PowerFireball>();
		if (fireball != null && fireball.Wielder.PlayerId != Wielder.PlayerId)
		{
			fireball.rigidbody2D.AddForce(fireball.rigidbody2D.velocity * -1);
			didReflect = true;
		}

		if (didReflect == true)
		{
			RunReflectAnimation();
		}
	}

	private void RunReflectAnimation()
	{
		TweenColor.Begin(gameObject, 0.2f, new Color(0.9f, 0.9f, 0.9f)).AddOnFinished(new EventDelegate(ReflectAnimationComplete));
	}

	private void ReflectAnimationComplete()
	{
		TweenColor.current.RemoveOnFinished(new EventDelegate(ReflectAnimationComplete));
	}

	private void PowerExpired()
	{
		GameObject.Destroy(gameObject);
	}
}

