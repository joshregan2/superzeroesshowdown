using UnityEngine;
using System.Collections;

public class PowerFireball : MonoBehaviour
{
	public const float FIREBALL_SPEED = 90.0f;
	public const int FIREBALL_DAMAGE = 50;

	public Character Wielder {get; set;}
	public ParticleSystem TrailParticleSystem;
	private EventTimer timer;
	private SpriteRenderer spriteRenderer;
	private Vector2 initialVelocity;
	private Vector2 direction;

	void Awake()
	{
		Rigidbody2D body = gameObject.AddComponent<Rigidbody2D>();
		body.drag = 0.1f;
		body.fixedAngle = true;
		body.gravityScale = 0;
	}

	// Use this for initialization
	void Start ()
	{
		spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
		spriteRenderer.sprite = Resources.Load<Sprite>("Powers/fireball");
		spriteRenderer.sortingOrder = 15;

		CircleCollider2D circleCollider = gameObject.AddComponent<CircleCollider2D>();
		circleCollider.isTrigger = true;

		GameObject trailGameObject = (GameObject)Instantiate(Resources.Load<GameObject>("Powers/fireball-trail-prefab"));
		TrailParticleSystem = trailGameObject.GetComponent<ParticleSystem>();
		TrailParticleSystem.renderer.sortingOrder = 15;
		trailGameObject.transform.position = gameObject.transform.position;
		trailGameObject.transform.parent = gameObject.transform;

		float angle = Mathf.Atan2(direction.y, direction.x);
		gameObject.transform.Rotate(new Vector3(0, 0, (angle*Mathf.Rad2Deg)-90));

		trailGameObject.transform.localPosition = new Vector3(0, -0.2f, 0);
		gameObject.rigidbody2D.velocity = initialVelocity;
		gameObject.rigidbody2D.AddForce(direction * FIREBALL_SPEED);

		timer = new EventTimer(false, new EventDelegate(PowerExpired));
		timer.StartTimer(PowersConfig.FIREBALL_DURATION);
	}

	// Update is called once per frame
	void Update ()
	{
		timer.Update();
	}

	IEnumerator FadeOut() {
		for (float f = 1f; f > -0.1f; f -= 0.2f) {
			Color c = spriteRenderer.material.color;
			c.a = f;
			spriteRenderer.material.color = c;
			TrailParticleSystem.renderer.material.color = c;
			if (f == 0)
			{
				GameObject.Destroy(gameObject);
			}
			yield return null;
		}
	}

	public void FireInDirection(Vector2 initialVelocity, Vector2 direction, Character wielder)
	{
		this.initialVelocity = initialVelocity;
		this.direction = direction;
		this.Wielder = wielder;
	}

	void OnTriggerEnter2D(Collider2D collider) 
	{
		Character character = collider.gameObject.GetComponent<Character>();
		Player player = collider.gameObject.GetComponent<Player>();
		if (character != null && player != null)
		{
			if (player.PlayerId != Wielder.PlayerId)
			{
				character.DamageCharacter(FIREBALL_DAMAGE, Wielder.PlayerId);

				PowerExpired();
			}
		}
	}

	private void PowerExpired()
	{
		GameObject.Destroy(gameObject);
	}
}

