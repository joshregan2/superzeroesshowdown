using UnityEngine;
using System.Collections;

public class DynamicShadow : MonoBehaviour
{
	private SpriteRenderer spriteRenderer;

	void Awake()
	{
		spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
	}

	// Use this for initialization
	void Start()
	{
		spriteRenderer.sprite = Resources.Load<Sprite>("Characters/placeholder-character-shadow");
		spriteRenderer.sortingOrder = 1;
	}

	// Update is called once per frame
	void Update()
	{

	}
}

