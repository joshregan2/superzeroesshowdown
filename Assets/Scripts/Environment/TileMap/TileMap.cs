using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class TileMap : MonoBehaviour
{
	public int Height {get; set;}
	public int Width {get; set;}	
	public int TileHeight {get; set;}
	public int TileWidth {get; set;}
	public int WidthInPixels {get; set;}
	public int HeightInPixels {get; set;}
	public List<RawTileSet> TileSets {get; set;}
	public List<TileLayer> TileLayers {get; set;}
	public List<Vector2> CollectibleSpawnLocations {get; set;}
	public List<Vector2> PlayerSpawnLocations {get; set;}

	private RawTileMap rawTileMap;

	void Awake()
	{
		TileLayers = new List<TileLayer>();
		CollectibleSpawnLocations = new List<Vector2>();
		PlayerSpawnLocations = new List<Vector2>();
	}

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	public int[,] Covert1DArrayTo2DArray(int[] src, int desiredWidth, int desiredHeight)
	{
		int[,] dest = new int[desiredWidth, desiredHeight];
		
		int count = src.Length;
		if (desiredWidth * desiredHeight < count)
		{
			count = desiredWidth * desiredHeight;
		}
		
		int index = 0;
		for (int y = 0; y < desiredHeight; y++)
		{
			for (int x = 0; x < desiredWidth; x++)
			{
				dest[x, y] = src[index++];
				if (index == count)
				{
					return dest;
				}
			}
		}
		return dest;
	}

	public void LoadJsonTilemap(string path)
	{
		TextAsset rawText = (TextAsset)Resources.Load(path, typeof(TextAsset));

		rawTileMap = (RawTileMap)JsonConvert.DeserializeObject<RawTileMap>(rawText.text);

		Height = rawTileMap.Height;
		Width = rawTileMap.Width;
		TileHeight = rawTileMap.TileHeight;
		TileWidth = rawTileMap.TileWidth;
		TileSets = rawTileMap.TileSets;
		WidthInPixels = Width * TileWidth;
		HeightInPixels = Height * TileHeight;
	
		Material spriteSnapMaterial = Resources.Load<Material>("Maps/SpriteSnap");

		List<Texture2D> tilesetTextures = new List<Texture2D>();
		for (int t = 0; t < TileSets.Count; t++)
		{
			tilesetTextures.Add(Resources.Load<Texture2D>("Maps/"+TileSets[0].Image.Replace(".png","")));
		}

		for (int i = 0; i < rawTileMap.Layers.Count; i++)
		{
			GameObject tileLayerGameObject = new GameObject(rawTileMap.Layers[i].Name);
			tileLayerGameObject.transform.parent = gameObject.transform;

			TileLayer tileLayer = tileLayerGameObject.AddComponent<TileLayer>();

			RawTileLayer rawLayer = rawTileMap.Layers[i];

			bool isOverlayLayer = rawLayer.Name.Contains("Overlay");
			bool isCollisionLayer = rawLayer.Name.Contains("Collision");
			bool isCollectibleLayer = rawLayer.Name.Contains("Collectible");
			bool isSpawnLayer = rawLayer.Name.Contains("Spawn");

			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					int tileIndex = rawLayer.Data[x + (y * Width)];

					if (tileIndex != 0)
					{
						float unitPerTileX = ((Camera.main.orthographicSize*2) * Camera.main.aspect) / (float)Width; 
						float unitPerTileY = (Camera.main.orthographicSize*2) / (float)Height;
						
						float positionX = -(Camera.main.orthographicSize * Camera.main.aspect) + ((float)x * unitPerTileX);
						float positionY = Camera.main.orthographicSize - ((float)y * unitPerTileY) - unitPerTileY;

						if (isCollectibleLayer == true)
						{
							CollectibleSpawnLocations.Add(new Vector2(positionX, positionY));
						}
						else if (isSpawnLayer == true)
						{
							PlayerSpawnLocations.Add(new Vector2(positionX, positionY));
						}
						else 
						{
							GameObject tileGameObject = new GameObject("Tile"+x.ToString()+y.ToString());
							tileGameObject.transform.parent = tileLayer.gameObject.transform;
							Tile tile = tileGameObject.AddComponent<Tile>();
			
							tileGameObject.transform.position = new Vector3(positionX, positionY, 1.0f);
							tileGameObject.transform.localScale = new Vector3(1.0f + (TileWidth/100.0f), 1.0f + (TileHeight/100.0f), 1.0f);

							int tilesetIndex = 0;
							int checkTileIndex = tileIndex;
							for (int ts = 0; ts < TileSets.Count; ts++)
							{
								checkTileIndex -= (TileSets[ts].ImageWidth / TileSets[ts].TileWidth) * (TileSets[ts].ImageHeight / TileSets[ts].TileHeight);
								if (checkTileIndex > 0)
								{
									tilesetIndex++;
								}
							}

							Texture2D texture = tilesetTextures[tilesetIndex];

							float tileX = (TileWidth * (tileIndex % (texture.width/TileWidth))) - TileWidth;
							float tileY = (TileHeight * (tileIndex / (texture.width/TileWidth)));
							tile.SetTileSprite(texture, tileX, tileY, TileWidth, TileHeight);
							tileLayer.Tiles.Add(tile);

							tile.SpriteRenderer.material = spriteSnapMaterial;
							tile.SpriteRenderer.sortingOrder = isOverlayLayer == true ? i+5 : i;

							if (isCollisionLayer == true)
							{
								BoxCollider2D boxCollider = tileGameObject.AddComponent<BoxCollider2D>();
								boxCollider.size = new Vector2(unitPerTileX, unitPerTileY);
							}
						}
					}
				}
			}
		}
	}

	public bool CheckIsCollidableTile(int x, int y)
	{
		RawTileLayer rawLayer = null;
		for (int i = 0; i < rawTileMap.Layers.Count; i++)
		{
			if (rawTileMap.Layers[i].Name.Contains("Collision") == true)
			{
				rawLayer = rawTileMap.Layers[i];
			}
		}

		if (rawLayer != null)
		{
			int tileX = x / 32;
			int tileY = y / 32;

			int tileIndex = rawLayer.Data[tileX + (tileY * rawTileMap.Width)];
			if (tileIndex == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		return false;
	}
}