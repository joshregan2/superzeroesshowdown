using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
	public SpriteRenderer SpriteRenderer {get; set;}

	void Awake()
	{
		SpriteRenderer = gameObject.AddComponent<SpriteRenderer>();
	}

	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{

	}

	public void SetTileSprite(Texture2D tileSetTexture, float rectX, float rectY, float rectWidth, float rectHeight)
	{
		SpriteRenderer.sprite = Sprite.Create(tileSetTexture, new Rect(rectX, tileSetTexture.height-rectY-rectHeight, rectWidth, rectHeight), new Vector2(0.0f, 0.0f));
	}
}

