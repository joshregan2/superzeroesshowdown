using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileLayer : MonoBehaviour
{
	public List<int> Data {get; set;}
	public int X {get; set;}
	public int Y {get; set;}
	public int Height {get; set;}
	public int Width {get; set;}
	public string Name {get; set;}
	public int Opacity {get; set;}
	public string Type {get; set;}
	public bool Visible {get; set;}
	public List<Tile> Tiles {get; set;}

	void Awake()
	{
		Tiles = new List<Tile>();
	}

	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{

	}

}

