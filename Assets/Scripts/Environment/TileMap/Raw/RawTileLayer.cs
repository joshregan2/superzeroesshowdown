using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class RawTileLayer
{
	[JsonProperty("data")]
	public List<int> Data {get; set;}

	[JsonProperty("x")]
	public int X {get; set;}

	[JsonProperty("y")]
	public int Y {get; set;}

	[JsonProperty("height")]
	public int Height {get; set;}

	[JsonProperty("width")]
	public int Width {get; set;}

	[JsonProperty("name")]
	public string Name {get; set;}

	[JsonProperty("opacity")]
	public int Opacity {get; set;}

	[JsonProperty("type")]
	public string Type {get; set;}

	[JsonProperty("visible")]
	public bool Visible {get; set;}

	public RawTileLayer()
	{

	}
}

