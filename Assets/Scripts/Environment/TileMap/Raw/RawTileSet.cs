using UnityEngine;
using System.Collections;
using Newtonsoft.Json;

public class RawTileSet
{
	[JsonProperty("firstgid")]
	public int FirstGid {get; set;}

	[JsonProperty("image")]
	public string Image {get; set;}

	[JsonProperty("imageheight")]
	public int ImageHeight {get; set;}

	[JsonProperty("imagewidth")]
	public int ImageWidth {get; set;}

	[JsonProperty("margin")]
	public int Margin {get; set;}

	[JsonProperty("name")]
	public string Name {get; set;}

	[JsonProperty("spacing")]
	public int Spacing {get; set;}

	[JsonProperty("tileheight")]
	public int TileHeight {get; set;}

	[JsonProperty("tilewidth")]
	public int TileWidth {get; set;}

	public RawTileSet()
	{

	}
}

