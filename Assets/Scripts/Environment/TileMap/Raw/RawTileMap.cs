using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class RawTileMap
{
	[JsonProperty("version")]
	public int Version {get; set;}	

	[JsonProperty("height")]
	public int Height {get; set;}

	[JsonProperty("width")]
	public int Width {get; set;}	
	
	[JsonProperty("tileheight")]
	public int TileHeight {get; set;}

	[JsonProperty("tilewidth")]
	public int TileWidth {get; set;}

	[JsonProperty("orientation")]
	public string Orientation {get; set;}

	[JsonProperty("layers")]
	public List<RawTileLayer> Layers {get; set;}

	[JsonProperty("tilesets")]
	public List<RawTileSet> TileSets {get; set;}

	public RawTileMap()
	{

	}
}

