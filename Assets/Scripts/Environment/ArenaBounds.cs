using UnityEngine;
using System.Collections;

public class ArenaBounds : MonoBehaviour
{
	public BoxCollider2D TopBounds {get; set;}
	public BoxCollider2D BottomBounds {get; set;}
	public BoxCollider2D LeftBounds {get; set;}
	public BoxCollider2D RightBounds {get; set;}

	void Awake()
	{

	}

	// Use this for initialization
	void Start()
	{
		float width = (Camera.main.orthographicSize*2) * Camera.main.aspect; 
		float height = (Camera.main.orthographicSize*2);
		ConfigureForBounds(width, height);
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void ConfigureForBounds(float width, float height)
	{
		float thickness = 0.5f;

		GameObject topBoundsGameObject = new GameObject("Top Bounds");
		topBoundsGameObject.transform.parent = gameObject.transform;
		topBoundsGameObject.transform.localPosition = new Vector3(0, (height / 2.0f) + (thickness/2.0f), 0);
		TopBounds = topBoundsGameObject.AddComponent<BoxCollider2D>();
		TopBounds.size = new Vector2(width, thickness);

		GameObject bottomBoundsGameObject = new GameObject("Bottom Bounds");
		bottomBoundsGameObject.transform.parent = gameObject.transform;
		bottomBoundsGameObject.transform.localPosition = new Vector3(0, -((height / 2.0f) + (thickness/2.0f)), 0);
		BottomBounds = bottomBoundsGameObject.AddComponent<BoxCollider2D>();
		BottomBounds.size = new Vector2(width, thickness);

		GameObject leftBoundsGameObject = new GameObject("Left Bounds");
		leftBoundsGameObject.transform.parent = gameObject.transform;
		leftBoundsGameObject.transform.localPosition = new Vector3(-((width / 2.0f) + (thickness/2.0f)),0, 0);
		LeftBounds = leftBoundsGameObject.AddComponent<BoxCollider2D>();
		LeftBounds.size = new Vector2(thickness, height);

		GameObject rightBoundsGameObject = new GameObject("Right Bounds");
		rightBoundsGameObject.transform.parent = gameObject.transform;
		rightBoundsGameObject.transform.localPosition = new Vector3((width / 2.0f) + (thickness/2.0f),0, 0);
		RightBounds = rightBoundsGameObject.AddComponent<BoxCollider2D>();
		RightBounds.size = new Vector2(thickness, height);
	}
}

