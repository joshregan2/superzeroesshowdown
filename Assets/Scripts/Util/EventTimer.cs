using UnityEngine;
using System.Collections;

public class EventTimer
{
	public bool TimerEnabled {get; set;}
	public float Duration {get; set;}
	public float CurrentTime {get; set;}
	public bool IsRecurring {get; set;}
	public EventDelegate TimerDelegate {get; set;}
	
	private bool isAnimating;

	public EventTimer(bool isRecurring, EventDelegate timerDelegate)
	{
		IsRecurring = isRecurring;
		TimerDelegate = timerDelegate;
		TimerEnabled = false;
	}

	public void Update()
	{
		if (TimerEnabled == true)
		{
			CurrentTime = Mathf.Max(CurrentTime - Time.deltaTime, 0);
			
			if (CurrentTime == 0)
			{
				if (IsRecurring == true)
				{
					CurrentTime = Duration;
				}
				else {
					TimerEnabled = false;
				}

				if (TimerDelegate != null)
				{
					TimerDelegate.Execute();
				}
			}
		}
	}
	
	public void StartTimer(float durationInSeconds)
	{
		Duration = durationInSeconds;
		CurrentTime = Duration;
		TimerEnabled = true;
	}

	public void StopTimer()
	{
		TimerEnabled = false;
	}
}

