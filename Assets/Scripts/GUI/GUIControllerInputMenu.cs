﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using InControl;

public class GUIControllerInputMenu : MonoBehaviour 
{
	public int SelectedIndex {get; set;}
	public List<UIButton> Allbuttons {get; set;}

	// Use this for initialization
	void Start () 
	{
		Allbuttons = new List<UIButton>(gameObject.GetComponentsInChildren<UIButton>());
		Reset();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (InputManager.Devices.Count > 0)
		{
			if (InputManager.ActiveDevice.Action1.WasReleased == true) 
			{
				Allbuttons[SelectedIndex].SendMessage("OnClick");
			}
			else if (InputManager.ActiveDevice.Action2.WasReleased == true) 
			{
				GameManager.SharedGameManager.GUIManager.PopScreen();
			}
			
			if (InputManager.ActiveDevice.LeftStickY.WasPressed == true && InputManager.ActiveDevice.LeftStickY < 0)
			{
				SelectPreviousButton();
			}
			
			if (InputManager.ActiveDevice.LeftStickY.WasPressed == true && InputManager.ActiveDevice.LeftStickY > 0)
			{
				SelectNextButton();
			}
		}
	}

	protected virtual void SelectNextButton()
	{
		if (SelectedIndex > 0)
		{
			SelectedIndex--;
			HighlightSelectedButton();
		}
	}

	protected virtual void SelectPreviousButton()
	{
		if (SelectedIndex < Allbuttons.Count-1)
		{
			SelectedIndex++;
			HighlightSelectedButton();
		}
	}

	protected virtual void HighlightSelectedButton()
	{
		if (Allbuttons != null)
		{
			for (int i = 0; i < Allbuttons.Count; i++)
			{
				if (Allbuttons[i] != null)
				{
					Allbuttons[i].SendMessage("OnHover", false);
				}
			}
			Allbuttons[SelectedIndex].SendMessage("OnHover", true);
		}
	}

	public void Reset()
	{
		SelectedIndex = 0;
		if (Input.GetJoystickNames().Length > 0)
		{
			HighlightSelectedButton();
		}
	}
}
