using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIPlayerResults : MonoBehaviour
{
	public UILabel ScoreValueLabel {get; set;}
	public UILabel AccoladeValueLabel {get; set;}
	public UILabel RankValueLabel {get; set;}

	void Awake()
	{
		UILabel[] labels = gameObject.GetComponentsInChildren<UILabel>();
		foreach (UILabel label in labels)
		{
			if (label.gameObject.name == "Score Value Label")
			{
				ScoreValueLabel = label;
			}
			else if (label.gameObject.name == "Accolade Value Label")
			{
				AccoladeValueLabel = label;
			}
			else if (label.gameObject.name == "Rank Value Label")
			{
				RankValueLabel = label;
			}
		}
	}

	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{

	}

	public void SetResults(int score, List<string> accolades, string rank)
	{
		ScoreValueLabel.text = score.ToString();
		string accoladesString = "";
		for (int i = 0; i < accolades.Count; i++)
		{
			accoladesString += accolades[i];

			if (i != accolades.Count - 1)
			{
				accoladesString += "/n";
			}
		}
		AccoladeValueLabel.text = accoladesString;

		RankValueLabel.text = rank;
	}
}

