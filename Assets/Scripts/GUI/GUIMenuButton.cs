using UnityEngine;
using System.Collections;

public class GUIMenuButton : MonoBehaviour
{
	public const string START_GAME_BUTTON = "Start Game Button";

	public const string SINGLEPLAYER_BUTTON = "Singleplayer Button";
	public const string MULTIPLAYER_BUTTON = "Multiplayer Button";
	public const string OPTIONS_BUTTON = "Options Button";

	public const string LOCAL_MULTIPLAYER_BUTTON = "Local Multiplayer Button";
	public const string NETWORK_MULTIPLAYER_BUTTON = "Network Multiplayer Button";

	public const string TEST_MODE_BUTTON = "Test Mode Button";

	public const string REMATCH_BUTTON = "Rematch Button";
	public const string MAIN_MENU_BUTTON = "Main Menu Button";

	public const string BACK_BUTTON = "Back Button";

	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{

	}

	void OnClick()
	{
		if (gameObject.activeSelf == true)
		{
			string gameObjectName = gameObject.name;

			if (gameObjectName == START_GAME_BUTTON)
			{
				GameManager.SharedGameManager.GUIManager.PushScreen(GameManager.SharedGameManager.GUIManager.MainMenuScreen);
			}
			else if (gameObjectName == SINGLEPLAYER_BUTTON)
			{
				GameManager.SharedGameManager.CurrentGameSession.ConfigureForSingleplayer();
				GameManager.SharedGameManager.GUIManager.PushScreen(GameManager.SharedGameManager.GUIManager.GameModeScreen);
			}
			else if (gameObjectName == MULTIPLAYER_BUTTON)
			{
				GameManager.SharedGameManager.GUIManager.PushScreen(GameManager.SharedGameManager.GUIManager.MultiplayerModeScreen);
			}
			else if (gameObjectName == OPTIONS_BUTTON)
			{
				GameManager.SharedGameManager.GUIManager.PushScreen(GameManager.SharedGameManager.GUIManager.OptionsScreen);
			}
			else if (gameObjectName == LOCAL_MULTIPLAYER_BUTTON)
			{
				GameManager.SharedGameManager.CurrentGameSession.ConfigureForLocalMultiplayer();
				GameManager.SharedGameManager.GUIManager.PushScreen(GameManager.SharedGameManager.GUIManager.GameModeScreen);
			}
			else if (gameObjectName == NETWORK_MULTIPLAYER_BUTTON)
			{
//				GameManager.SharedGameManager.CurrentGameSession.ConfigureForNetworkMultiplayer();
//				GameManager.SharedGameManager.GUIManager.PushScreen(GameManager.SharedGameManager.GUIManager.GameModeScreen);
			}
			else if (gameObjectName == TEST_MODE_BUTTON)
			{
				GameManager.SharedGameManager.GUIManager.PushScreen(GameManager.SharedGameManager.GUIManager.CharacterSelectionScreen);
			}
			else if (gameObjectName == REMATCH_BUTTON)
			{
				GameManager.SharedGameManager.CurrentGameSession.ResetAllPlayers();
				GameManager.SharedGameManager.GUIManager.PopToScreen(GameManager.SharedGameManager.GUIManager.GameHUDScreen);
				GameManager.SharedGameManager.GUIManager.UpdateActivePlayerHUDForGameSession();
				GameManager.SharedGameManager.LoadGameSession();
			}
			else if (gameObjectName == MAIN_MENU_BUTTON)
			{
				GameManager.SharedGameManager.InitialiseGameSession();
				GameManager.SharedGameManager.GUIManager.PopToScreen(GameManager.SharedGameManager.GUIManager.MainMenuScreen);
			}
			else if (gameObjectName == BACK_BUTTON)
			{
				GameManager.SharedGameManager.GUIManager.PopScreen();
			}
		}
	}

	void OnHover(bool isOver)
	{

	}
}

