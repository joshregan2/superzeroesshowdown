using UnityEngine;
using System.Collections;

public class GUIGameTimer : MonoBehaviour
{
	public UILabel TimerLabel {get; set;}
	public bool TimerEnabled {get; set;}
	public float Duration {get; set;}
	public float CurrentTime {get; set;}
	public bool TimerComplete {get; set;}

	private bool isAnimating;

	// Use this for initialization
	void Start ()
	{
		TimerEnabled = false;
		TimerComplete = false;
		TimerLabel = gameObject.GetComponent<UILabel>();
		isAnimating = false;
	}

	// Update is called once per frame
	void Update ()
	{
		if (TimerEnabled == true)
		{
			CurrentTime = Mathf.Max(CurrentTime - Time.deltaTime, 0);

			int minutes = (int)(CurrentTime - (CurrentTime % 60)) / 60;
			int seconds = (int)(CurrentTime % 60);
			TimerLabel.text = minutes.ToString()+":"+seconds.ToString();

			if (CurrentTime == 0)
			{
				TimerEnabled = false;
				TimerComplete = true;
			}
			else if (CurrentTime <= Duration*0.05f && isAnimating == false) // Animate on last 5%
			{
				isAnimating = true;
				TweenScale.Begin(gameObject, 0.4f, new Vector3(1.25f, 1.25f, 1.0f)).AddOnFinished(new EventDelegate(AnimationScaleUpComplete));
			}
		}
	}

	private void AnimationScaleUpComplete()
	{
		TweenScale.current.RemoveOnFinished(new EventDelegate(AnimationScaleUpComplete));
		TweenScale.Begin(gameObject, 0.4f, new Vector3(1.0f, 1.0f, 1.0f)).AddOnFinished(new EventDelegate(AnimationScaleDownComplete));
	}

	private void AnimationScaleDownComplete()
	{
		TweenScale.current.RemoveOnFinished(new EventDelegate(AnimationScaleDownComplete));
		isAnimating = false;
	}

	public void StartTimer(float durationInSeconds)
	{
		TimerComplete = false;
		Duration = durationInSeconds;
		CurrentTime = Duration;
		TimerEnabled = true;
	}
}

