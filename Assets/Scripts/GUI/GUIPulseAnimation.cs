using UnityEngine;
using System.Collections;

public class GUIPulseAnimation : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		RunPulseAnimation();
	}

	// Update is called once per frame
	void Update ()
	{

	}

	public void RunPulseAnimation()
	{
		if (TweenScale.current != null)
		{
			TweenScale.current.RemoveOnFinished(new EventDelegate(RunPulseAnimation));
		}

		TweenScale scaleTween =  TweenScale.Begin(gameObject, 0.6f, new Vector3(1.05f, 1.05f, 1.0f));
		scaleTween.AddOnFinished(new EventDelegate(PulseUpComplete));
	}

	private void PulseUpComplete()
	{
		TweenScale.current.RemoveOnFinished(new EventDelegate(PulseUpComplete));

		TweenScale scaleTween =  TweenScale.Begin(gameObject, 0.6f, new Vector3(1.0f, 1.0f, 1.0f));
		scaleTween.AddOnFinished(new EventDelegate(RunPulseAnimation));
	}
}

