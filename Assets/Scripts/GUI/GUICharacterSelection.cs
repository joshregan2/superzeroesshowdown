﻿using UnityEngine;
using System.Collections;

public class GUICharacterSelection : MonoBehaviour 
{
	public UITexture BackgroundTexture {get; set;}
	public UILabel NameLabel {get; set;}

	void Awake()
	{
		
	}

	// Use this for initialization
	void Start() 
	{
		BackgroundTexture = gameObject.GetComponentInChildren<UITexture>();
		NameLabel = BackgroundTexture.gameObject.GetComponentInChildren<UILabel>();
	}
	
	// Update is called once per frame
	void Update() 
	{
	
	}

	public void ShowSelectedByPlayer(int playerId)
	{
		
	}
}
