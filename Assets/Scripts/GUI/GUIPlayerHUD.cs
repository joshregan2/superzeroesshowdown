﻿using UnityEngine;
using System.Collections;

public class GUIPlayerHUD : MonoBehaviour 
{
	public UILabel TitleLabel {get; set;}
	public UILabel HealthLabel {get; set;}
	public UILabel PowerLabel {get; set;}
	public UILabel ScoreLabel {get; set;}

	void Awake() 
	{
		UILabel[] labels = gameObject.GetComponentsInChildren<UILabel>();
		foreach (UILabel label in labels)
		{
			if (label.gameObject.name == "Title Label")
			{
				TitleLabel = label;
			}
			else if (label.gameObject.name == "Health Label")
			{
				HealthLabel = label;	
			}
			else if (label.gameObject.name == "Power Label")
			{
				PowerLabel = label;
			}
			else if (label.gameObject.name == "Score Label")
			{
				ScoreLabel = label;
			}
		}
	}

	// Use this for initialization
	void Start() 
	{
	
	}
	
	// Update is called once per frame
	void Update() 
	{
	
	}

	public void SetPlayerName(string name)
	{
		TitleLabel.text = "P1 - "+name;
	}

	public void SetHealth(int health, int maxHealth)
	{
		HealthLabel.text = "Health: "+health.ToString()+"/"+maxHealth.ToString();
	}

	public void SetPower(PowersConfig.PowerTypes powerType)
	{
		string powerName = "";

		switch(powerType)
		{
			case PowersConfig.PowerTypes.EMPTY:
				powerName = "None";
				break;
			case PowersConfig.PowerTypes.FIREBALL:
				powerName = "Fireball";
				break;
			case PowersConfig.PowerTypes.SHIELD:
				powerName = "Shield";
				break;
			case PowersConfig.PowerTypes.SUPER_SPEED:
				powerName = "Super Speed";
				break;
		}

		PowerLabel.text = "Power: "+powerName;
	}

	public void SetScore(int score)
	{
		ScoreLabel.text = "Score: "+score.ToString();
	}
}
