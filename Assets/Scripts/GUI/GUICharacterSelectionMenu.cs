﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using InControl;

public class GUICharacterSelectionMenu : MonoBehaviour 
{
	public List<GUICharacterSelection> AllCharacterSelections {get; set;}

	public Dictionary<int, int> SelectedIndex {get; set;}
	public Dictionary<int, UITexture> PlayerMarkers {get; set;}
	public Dictionary<int, UITexture> PlayerConfirmedMarkers {get; set;}

	void Awake()
	{

	}

	// Use this for initialization
	void Start() 
	{
		AllCharacterSelections = new List<GUICharacterSelection>(gameObject.GetComponentsInChildren<GUICharacterSelection>());

		Reset();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (gameObject.activeSelf == true)
		{
			if (InputManager.Devices.Count > 0 && GameManager.SharedGameManager.CurrentGameSession.GameMode == GlobalConfig.GameModes.LOCAL_MULTIPLAYER)
			{
				if (InputManager.ActiveDevice.Action2.WasReleased == true) 
				{
					GameManager.SharedGameManager.GUIManager.PopScreen();
				}

				foreach (KeyValuePair<int, PlayerStats> kvp in GameManager.SharedGameManager.CurrentGameSession.ActivePlayers)
				{
					int playerId = kvp.Key;
					if (playerId == 1 || InputManager.Devices.Count >= playerId)
					{
						if (playerId != 1 && InputManager.Devices[playerId-1].Action4.WasReleased == true && kvp.Value.IsActive == false) 
						{
							kvp.Value.IsActive = true;
							SelectedIndex.Add(playerId, 0);
							PlayerMarkers.Add(playerId, CreatePlayerMarker(playerId));
						}

						if (kvp.Value.IsActive == true)
						{
							if (PlayerConfirmedMarkers.ContainsKey(playerId) == false)
							{
								if (InputManager.Devices[playerId-1].Action1.WasReleased) 
								{
									ConfirmSelection(playerId);
								}

								if (InputManager.Devices[playerId-1].LeftStickY.WasPressed == true && InputManager.Devices[playerId-1].LeftStickY < 0)
								{
									SelectPreviousCharacter(playerId);
								}
								
								if (InputManager.Devices[playerId-1].LeftStickY.WasPressed == true && InputManager.Devices[playerId-1].LeftStickY > 0)
								{
									SelectNextCharacter(playerId);
								}
							}
						}
					}
				}
			}
			else if (InputManager.Devices.Count > 0)
			{
				if (InputManager.Devices[0].Action2.WasReleased == true) 
				{
					GameManager.SharedGameManager.GUIManager.PopScreen();
				}

				if (PlayerConfirmedMarkers.ContainsKey(1) == false)
				{
					if (InputManager.Devices[0].Action1.WasReleased) 
					{
						ConfirmSelection(1);
					}
					
					if (InputManager.Devices[0].LeftStickY.WasPressed == true && InputManager.Devices[0].LeftStickY < 0)
					{
						SelectPreviousCharacter(1);
					}
					
					if (InputManager.Devices[0].LeftStickY.WasPressed == true && InputManager.Devices[0].LeftStickY > 0)
					{
						SelectNextCharacter(1);
					}
				}
			}
			else
			{
				if (PlayerConfirmedMarkers.ContainsKey(1) == false)
				{
					if (Input.GetKeyDown(KeyCode.Space)) 
					{
						ConfirmSelection(1);
					}
					
					if (Input.GetKeyDown(KeyCode.S))
					{
						SelectPreviousCharacter(1);
					}
					
					if (Input.GetKeyDown(KeyCode.W))
					{
						SelectNextCharacter(1);
					}
				}
			}
		}
	}

	public void Reset()
	{
		if (PlayerMarkers != null)
		{
			foreach (KeyValuePair<int, UITexture> kvp in PlayerMarkers)
			{
				if (kvp.Value != null)
				{
					GameObject.Destroy(kvp.Value.gameObject);
				}
			}
		}
		if (PlayerConfirmedMarkers != null)
		{
			foreach (KeyValuePair<int, UITexture> kvp in PlayerConfirmedMarkers)
			{
				if (kvp.Value != null)
				{
					GameObject.Destroy(kvp.Value.gameObject);
				}
			}
		}
		SelectedIndex = new Dictionary<int, int>();
		PlayerMarkers = new Dictionary<int, UITexture>();
		PlayerConfirmedMarkers = new Dictionary<int, UITexture>();
		SelectedIndex.Add(1, 0);
		PlayerMarkers.Add(1, CreatePlayerMarker(1));
		
		if (Input.GetJoystickNames().Length > 0)
		{
			HighlightSelectedCharacter(0);
		}
	}
	
	private void SelectNextCharacter(int playerId)
	{
		if (SelectedIndex[playerId] > 0)
		{
			SelectedIndex[playerId]--;
			HighlightSelectedCharacter(playerId);
		}
	}
	
	private void SelectPreviousCharacter(int playerId)
	{
		if (SelectedIndex[playerId] < AllCharacterSelections.Count-1)
		{
			SelectedIndex[playerId]++;
			HighlightSelectedCharacter(playerId);
		}
	}
	
	private void HighlightSelectedCharacter(int playerId)
	{
		if (PlayerMarkers.ContainsKey(playerId) == true && PlayerMarkers[playerId] != null)
		{
			GameObject.Destroy(PlayerMarkers[playerId].gameObject);
			PlayerMarkers[playerId] = CreatePlayerMarker(playerId);
		}
	}

	private void CancelSelection(int playerId)
	{
		if (PlayerConfirmedMarkers.ContainsKey(playerId) == true && PlayerConfirmedMarkers[playerId] != null)
		{
			GameObject.Destroy(PlayerConfirmedMarkers[playerId].gameObject);
			PlayerConfirmedMarkers.Remove(playerId);
		}

		PlayerMarkers[playerId] = CreatePlayerMarker(playerId);
	}

	private void ConfirmSelection(int playerId)
	{
		if (PlayerMarkers.ContainsKey(playerId) == true && PlayerMarkers[playerId] != null)
		{
			GameObject.Destroy(PlayerMarkers[playerId].gameObject);
			PlayerMarkers[playerId] = null;
 		}
		
		if (PlayerConfirmedMarkers.ContainsKey(playerId) == true && PlayerConfirmedMarkers[playerId] != null)
		{
			GameObject.Destroy(PlayerConfirmedMarkers[playerId].gameObject);
			PlayerConfirmedMarkers.Remove(playerId);
		}

		PlayerConfirmedMarkers.Add(playerId, CreateConfirmedPlayerMarker(playerId));

		// If singleplayer setup AI
		if (GameManager.SharedGameManager.CurrentGameSession.GameMode == GlobalConfig.GameModes.SINGLEPLAYER)
		{

		}

		if (GameManager.SharedGameManager.CurrentGameSession.GetActivePlayersCount() == PlayerConfirmedMarkers.Count)
		{
			GameManager.SharedGameManager.GUIManager.PushScreen(GameManager.SharedGameManager.GUIManager.GameHUDScreen);
			GameManager.SharedGameManager.GUIManager.UpdateActivePlayerHUDForGameSession();
			GameManager.SharedGameManager.LoadGameSession();
		}
	}

	private UITexture CreatePlayerMarker(int playerId)
	{
		UITexture playerMarker = NGUITools.AddWidget<UITexture>(AllCharacterSelections[SelectedIndex[playerId]].gameObject);
		playerMarker.mainTexture = Resources.Load<Texture2D>("UI/player-marker");
		playerMarker.SetDimensions(playerMarker.mainTexture.width, playerMarker.mainTexture.height);
		playerMarker.depth = 6;

		return playerMarker;
	}

	private UITexture CreateConfirmedPlayerMarker(int playerId)
	{
		UITexture playerMarker = NGUITools.AddWidget<UITexture>(AllCharacterSelections[SelectedIndex[playerId]].gameObject);
		playerMarker.mainTexture = Resources.Load<Texture2D>("UI/player-marker-confirmed");
		playerMarker.SetDimensions(playerMarker.mainTexture.width, playerMarker.mainTexture.height);
		playerMarker.depth = 6;
		
		return playerMarker;
	}
}
