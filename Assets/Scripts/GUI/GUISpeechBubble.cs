﻿using UnityEngine;
using System.Collections;

public class GUISpeechBubble : MonoBehaviour 
{

	// Use this for initialization
	void Start()
	{
		RunPopAnimation();
	}
	
	// Update is called once per frame
	void Update() 
	{
	
	}

	public void RunPopAnimation()
	{
		gameObject.transform.localScale = new Vector3(0, 0, 1.0f);
		TweenScale scaleTween = TweenScale.Begin(gameObject, 0.2f, new Vector3(1.2f, 1.2f, 1.0f));
		scaleTween.AddOnFinished(new EventDelegate(ScaleUpComplete));
		scaleTween.delay = 0.4f;
	}

	private void ScaleUpComplete()
	{
		TweenScale.current.RemoveOnFinished(new EventDelegate(ScaleUpComplete));
		TweenScale scaleTween = TweenScale.Begin(gameObject, 0.05f, new Vector3(1.0f, 1.0f, 1.0f));
		scaleTween.AddOnFinished(new EventDelegate(ScaleUpComplete));
		scaleTween.delay = 0;
	}
}
