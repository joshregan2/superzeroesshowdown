using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerStats
{
	public int PlayerId {get; set;}
	public bool IsActive {get; set;}
	public bool IsComputer {get; set;}
	public int Score {get; set;}
	public List<string> Accolades {get; set;}

	public PlayerStats(int playerId)
	{
		PlayerId = playerId;
		IsActive = false;
		IsComputer = false;
		Score = 0;
		Accolades = new List<string>();
	}
}

