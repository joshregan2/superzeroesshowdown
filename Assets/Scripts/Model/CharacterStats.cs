using UnityEngine;
using System.Collections;

public class CharacterStats
{
	public string Name {get; set;}
	public string SpriteFile {get; set;}
	public float MoveSpeed {get; set;}
	public float DragAmount {get; set;}
	public int MaxHealth {get; set;}

	public CharacterStats()
	{

	}

	public CharacterStats(string name, string spriteFile, float moveSpeed, float dragAmount, int maxHealth)
	{
		Name = name;
		SpriteFile = spriteFile;
		MoveSpeed = moveSpeed;
		MaxHealth = maxHealth;
		DragAmount = dragAmount;
	}

	public CharacterStats Clone()
	{
		return (CharacterStats)this.MemberwiseClone();
	}
}