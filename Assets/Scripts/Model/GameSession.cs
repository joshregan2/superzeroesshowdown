using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSession
{
	public GlobalConfig.GameModes GameMode { get; set; }
	public Dictionary<int, PlayerStats> ActivePlayers {get; set;}
	public bool GameInProgress {get; set;}

	public GameSession()
	{
		ActivePlayers = new Dictionary<int, PlayerStats>();
		GameInProgress = false;
	}

	public GameSession(GlobalConfig.GameModes gameMode) : this()
	{
		GameMode = gameMode;
	}

	public void ConfigureForSingleplayer()
	{
		GameMode = GlobalConfig.GameModes.SINGLEPLAYER;
		ActivePlayers.Clear();
		for (int i = 0; i < GlobalConfig.MAX_LOCAL_PLAYERS; i++)
		{
			PlayerStats stats = new PlayerStats(i+1);
			stats.IsActive = true;
			if (i != 0)
			{
				stats.IsComputer = true;
			}
			ActivePlayers.Add(i+1, stats);
		}
	}

	public void ConfigureForLocalMultiplayer()
	{
		GameMode = GlobalConfig.GameModes.LOCAL_MULTIPLAYER;
		ActivePlayers.Clear();
		for (int i = 0; i < GlobalConfig.MAX_LOCAL_PLAYERS; i++)
		{
			ActivePlayers.Add(i+1, new PlayerStats(i+1));
		}
		ActivePlayers[1].IsActive = true;
	}

	public void ConfigureForNetworkMultiplayer()
	{
		GameMode = GlobalConfig.GameModes.NETWORK_MULTIPLAYER;
		ActivePlayers.Clear();
		for (int i = 0; i < GlobalConfig.MAX_NETWORK_PLAYERS; i++)
		{
			ActivePlayers.Add(i+1, new PlayerStats(i+1));
		}
		ActivePlayers[1].IsActive = true;
	}

	public void AddAIPlayer()
	{

	}

	public int GetActivePlayersCount()
	{
		int count = 0;
		foreach (KeyValuePair<int, PlayerStats> kvp in ActivePlayers)
		{
			if (kvp.Value.IsActive == true && kvp.Value.IsComputer == false)
			{
				count++;
			}
		}
		return count;
	}

	public void ResetAllPlayers()
	{
		foreach (KeyValuePair<int, PlayerStats> kvp in ActivePlayers)
		{
			if (kvp.Value.IsActive == true)
			{
				kvp.Value.Score = 0;
				kvp.Value.Accolades = new List<string>();
			}
		}
	}

	public void ResetScoreForPlayer(int playerId)
	{
		if (ActivePlayers.ContainsKey(playerId))
		{
			ActivePlayers[playerId].Score = 0;
		}
	}

	public void AddPointToScoreForPlayer(int playerId)
	{
		if (ActivePlayers.ContainsKey(playerId))
		{
			ActivePlayers[playerId].Score++;
			GameManager.SharedGameManager.GUIManager.GetPlayerHUDForPlayerId(playerId).SetScore(ActivePlayers[playerId].Score);
		}
	}
}

