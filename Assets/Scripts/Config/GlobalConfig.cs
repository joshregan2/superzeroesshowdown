using UnityEngine;
using System.Collections;

public class GlobalConfig
{
	public const int MAX_LOCAL_PLAYERS = 4;
	public const int MAX_NETWORK_PLAYERS = 4;

	public enum GameModes
	{
		SINGLEPLAYER,
		LOCAL_MULTIPLAYER,
		NETWORK_MULTIPLAYER
	};

	public const float DEFAULT_GAME_DURATION = 1 * 60;
	public const int CHARACTER_RESPAWN_TIMER = 100;

}

