using UnityEngine;
using System.Collections;

public class PowersConfig
{
	/* Power Types */
	public const int POWER_TYPES_COUNT = 4;
	public enum PowerTypes
	{
		EMPTY,
		FIREBALL,
		SHIELD,
		SUPER_SPEED
	};

	/* Power Durations (in seconds) */
	public const int FIREBALL_DURATION = 3;
	public const int SHIELD_DURATION = 5;
	public const int SUPER_SPEED_DURATION = 5;


	public PowersConfig()
	{

	}
}

