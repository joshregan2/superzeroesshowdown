using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterConfig
{
	public enum CharacterTypes
	{
		TEST_CHARACTER,
		TEST_CHARACTER_CIRCLE
	};

	public enum CharacterStates
	{
		ACTIVE,
		RESPAWNING
	};

	public static readonly Dictionary<CharacterConfig.CharacterTypes, CharacterStats> CharacterStatsDefinitions = new Dictionary<CharacterTypes, CharacterStats>()
	{
		{ CharacterConfig.CharacterTypes.TEST_CHARACTER, new CharacterStats("Test Character", "Characters/placeholder-character", 18.0f, 18.0f, 100) },
		{ CharacterConfig.CharacterTypes.TEST_CHARACTER_CIRCLE, new CharacterStats("Test Character", "Characters/placeholder-circle-character", 32.0f, 18.0f, 100) }
	};
}

