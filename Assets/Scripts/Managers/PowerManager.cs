using UnityEngine;
using System.Collections;

public class PowerManager : MonoBehaviour
{
	void Awake()
	{

	}

	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{

	}

	public void ExecutePower(Character wielder, PowersConfig.PowerTypes powerType)
	{
		if (powerType == PowersConfig.PowerTypes.FIREBALL)
		{
			ManifestFireballPower(wielder);
		}
		else if (powerType == PowersConfig.PowerTypes.SHIELD)
		{
			ManifestShieldPower(wielder);
		}
		else if (powerType == PowersConfig.PowerTypes.SUPER_SPEED)
		{
			ManifestSuperSpeedPower(wielder);
		}
	}

	private void ManifestFireballPower(Character wielder)
	{
		GameObject fireballGameObject = new GameObject("Fireball");
		PowerFireball powerFireball = fireballGameObject.AddComponent<PowerFireball>();
		fireballGameObject.transform.position = wielder.gameObject.transform.position;
		powerFireball.FireInDirection(wielder.gameObject.rigidbody2D.velocity, wielder.CurrentHeading, wielder);
	}

	private void ManifestShieldPower(Character wielder)
	{
		GameObject shieldGameObject = new GameObject("Shield");
		PowerShield powerShield = shieldGameObject.AddComponent<PowerShield>();
		powerShield.Wielder = wielder;
		shieldGameObject.transform.position = wielder.gameObject.transform.position;
		shieldGameObject.transform.parent = wielder.gameObject.transform;
	}

	private void ManifestSuperSpeedPower(Character wielder)
	{
		GameObject superSpeedGameObject = new GameObject("Super Speed");
		PowerSuperSpeed powerSuperSpeed = superSpeedGameObject.AddComponent<PowerSuperSpeed>();
		powerSuperSpeed.Wielder = wielder;
		superSpeedGameObject.transform.position = wielder.gameObject.transform.position;
		superSpeedGameObject.transform.parent = wielder.gameObject.transform;
	}
}

