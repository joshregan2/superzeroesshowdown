using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnvironmentManager : MonoBehaviour 
{
	public TileMap CurrentTileMap {get; set;}
	public ArenaBounds ArenaBounds {get; set;}
	public List<Collectible> AllCollectibles {get; set;}

	private const int MAX_COLLECTIBLE_SPAWN = 6;
	private const int COLLECTIBLE_SPAWN_TIME = 100;
	private int collectibleSpawnCounter;


	void Awake()
	{
		AllCollectibles = new List<Collectible>();
		collectibleSpawnCounter = 0;
	}

	void Start()
	{

	}

	void Update()
	{
		if (GameManager.SharedGameManager.CurrentGameSession.GameInProgress == true &&
		    CurrentTileMap != null && CurrentTileMap.CollectibleSpawnLocations.Count > 0 &&
		    AllCollectibles.Count < MAX_COLLECTIBLE_SPAWN)
		{
		    if (collectibleSpawnCounter < COLLECTIBLE_SPAWN_TIME)
			{
				collectibleSpawnCounter++;
			}
			else 
			{
				collectibleSpawnCounter = 0;
				GenerateCollectible();
			}
		}
	}

	public void LoadTileMap()
	{
		GameObject tileMapGameObject = new GameObject("Tile Map");
		CurrentTileMap = tileMapGameObject.AddComponent<TileMap>();
		CurrentTileMap.LoadJsonTilemap("Maps/test-map-arena4");

		GameObject arenaBoundsGameObject = new GameObject("Arena Bounds");
		ArenaBounds = arenaBoundsGameObject.AddComponent<ArenaBounds>();
	}

	public void GenerateCollectible()
	{
		Vector2 spawnLocation = NextAvailableCollectibleSpawnLocation();

		GameObject collectibleObject = new GameObject("Power Up");
		collectibleObject.transform.position = new Vector3(spawnLocation.x, spawnLocation.y, 0);
		Collectible collectible = collectibleObject.AddComponent<Collectible>();
		collectible.ConfigureForPower((PowersConfig.PowerTypes)Random.Range(1, PowersConfig.POWER_TYPES_COUNT));
		AllCollectibles.Add(collectible);
	}

	public Vector2 NextAvailableCollectibleSpawnLocation()
	{
		Vector2 spawnLocation = CurrentTileMap.CollectibleSpawnLocations[Random.Range(0, CurrentTileMap.CollectibleSpawnLocations.Count-1)];
		
		return spawnLocation;
	}

	public Vector2 NextAvailablePlayerSpawnLocation()
	{
		Vector2 spawnLocation = CurrentTileMap.PlayerSpawnLocations[Random.Range(0, CurrentTileMap.PlayerSpawnLocations.Count-1)];

		return spawnLocation;
	}

	public void DestroyEnvironment()
	{
		GameObject.Destroy(CurrentTileMap.gameObject);
		GameObject.Destroy(ArenaBounds.gameObject);
		for (int i = 0; i < AllCollectibles.Count; i++)
		{
			GameObject.Destroy(AllCollectibles[i].gameObject);
		}
		AllCollectibles = new List<Collectible>();
	}
}

