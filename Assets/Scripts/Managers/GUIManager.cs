﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIManager : MonoBehaviour 
{
	/* Screen Management */
	private Stack<UIWidget> navigationStack;

	/* Title Screen */
	public UIWidget TitleScreen { get; set; }

	/* Main Menu Screen */
	public UIWidget MainMenuScreen {get; set;}
	public UIWidget MainMenu {get; set;}

	/* Multiplayer Mode Screen */
	public UIWidget MultiplayerModeScreen {get; set;}

	/* Options Screen */
	public UIWidget OptionsScreen {get; set;}

	/* Character Selection Screen */
	public UIWidget CharacterSelectionScreen {get; set;}
	public GUICharacterSelectionMenu CharacterSelectionMenu {get; set;}

	/* Game Mode Selection Screen */
	public UIWidget GameModeScreen { get; set; }

	/* Map Selection Screen */
	public UIWidget MapSelectionScreen {get; set;}

	/* Match Making Screen */
	public UIWidget MatchMakingScreen {get; set;}

	/* Player HUDS */
	public UIWidget GameHUDScreen {get; set;}
	public GUIGameTimer GameTimer {get; set;}
	public GUIPlayerHUD Player1HUD {get; set;}
	public GUIPlayerHUD Player2HUD {get; set;}
	public GUIPlayerHUD Player3HUD {get; set;}
	public GUIPlayerHUD Player4HUD {get; set;}

	/* Game Results */
	public UIWidget ResultsScreen {get; set;}
	public GUIPlayerResults Player1Results {get; set;}
	public GUIPlayerResults Player2Results {get; set;}
	public GUIPlayerResults Player3Results {get; set;}
	public GUIPlayerResults Player4Results {get; set;}

	void Awake ()
	{
		navigationStack = new Stack<UIWidget>();
	}

	// Use this for initialization
	void Start () 
	{
		ObtainGUIReferences();
		SetDefaults();

		// Start the game by showing the title screen
		PushScreen(TitleScreen);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	private void ObtainGUIReferences()
	{
		// Title Screen
		TitleScreen = GameObject.Find("Title Screen").GetComponent<UIWidget>();

		// Main Menu
		MainMenuScreen = GameObject.Find("Main Menu Screen").GetComponent<UIWidget>();
		MainMenu = GameObject.Find("Main Menu").GetComponent<UIWidget>();

		// Multiplayer Mode Screen
		MultiplayerModeScreen = GameObject.Find("Multiplayer Mode Screen").GetComponent<UIWidget>();

		// Options Screen
		OptionsScreen = GameObject.Find("Options Screen").GetComponent<UIWidget>();

		// Character Selection Screen
		CharacterSelectionScreen = GameObject.Find("Character Selection Screen").GetComponent<UIWidget>();
		CharacterSelectionMenu = GameObject.Find("Character Selection Menu").GetComponent<GUICharacterSelectionMenu>();

		// Game Mode Selection Screen
		GameModeScreen = GameObject.Find("Game Mode Screen").GetComponent<UIWidget>();

		// Map Selection Screen
		MapSelectionScreen = GameObject.Find("Map Selection Screen").GetComponent<UIWidget>();

		// Match Making Screen
		MatchMakingScreen = GameObject.Find("Match Making Screen").GetComponent<UIWidget>();

		// Player HUDs
		GameHUDScreen = GameObject.Find("Game HUD").GetComponent<UIWidget>();
		GameTimer = GameObject.Find("Game Timer Label").GetComponent<GUIGameTimer>();
		Player1HUD = GameObject.Find("Player 1 HUD").GetComponent<GUIPlayerHUD>();
		Player2HUD = GameObject.Find("Player 2 HUD").GetComponent<GUIPlayerHUD>();
		Player3HUD = GameObject.Find("Player 3 HUD").GetComponent<GUIPlayerHUD>();
		Player4HUD = GameObject.Find("Player 4 HUD").GetComponent<GUIPlayerHUD>();

		// Results Screen
		ResultsScreen = GameObject.Find("Results Screen").GetComponent<UIWidget>();
		Player1Results = GameObject.Find("Player 1 Results").GetComponent<GUIPlayerResults>();
		Player2Results = GameObject.Find("Player 2 Results").GetComponent<GUIPlayerResults>();
		Player3Results = GameObject.Find("Player 3 Results").GetComponent<GUIPlayerResults>();
		Player4Results = GameObject.Find("Player 4 Results").GetComponent<GUIPlayerResults>();
	}

	private void SetDefaults()
	{
		NGUITools.SetActive(MainMenuScreen.gameObject, false);
		NGUITools.SetActive(MultiplayerModeScreen.gameObject, false);
		NGUITools.SetActive(OptionsScreen.gameObject, false);
		NGUITools.SetActive(CharacterSelectionScreen.gameObject, false);
		NGUITools.SetActive(GameModeScreen.gameObject, false);
		NGUITools.SetActive(MapSelectionScreen.gameObject, false);
		NGUITools.SetActive(MatchMakingScreen.gameObject, false);
		NGUITools.SetActive(GameHUDScreen.gameObject, false);
		NGUITools.SetActive(ResultsScreen.gameObject, false);
	}

	public void HideScreen(UIWidget screenWidget)
	{
		screenWidget.alpha = 0;
		NGUITools.SetActive(screenWidget.gameObject, false);
	}

	public void ShowScreen(UIWidget screenWidget)
	{
		screenWidget.alpha = 1.0f;
		NGUITools.SetActive(screenWidget.gameObject, true);

		GUIControllerInputMenu[] controllerInputMenus = screenWidget.gameObject.GetComponentsInChildren<GUIControllerInputMenu>();
		foreach (GUIControllerInputMenu inputMenu in controllerInputMenus)
		{
			inputMenu.Reset();
		}
	}

	public void PushScreen(UIWidget screen)
	{
		if (navigationStack.Count > 0)
		{
			HideScreen(navigationStack.Peek());
		}
		navigationStack.Push(screen);
		ShowScreen(screen);
	}

	public void PopScreen()
	{
		if (navigationStack.Count > 1)
		{
			HideScreen(navigationStack.Pop());

			ShowScreen(navigationStack.Peek());
		}
	}

	public void PopToScreen(UIWidget screen)
	{
		if (navigationStack.Count > 1 && navigationStack.Contains(screen) == true)
		{
			while(navigationStack.Peek() != screen)
			{
				HideScreen(navigationStack.Pop());
			}
			
			ShowScreen(navigationStack.Peek());
		}
	}

	public void ClearNavigationStack()
	{
		navigationStack.Clear();
	}

	public void TransitionMainMenuToGameScreen()
	{
		if (navigationStack.Count > 0)
		{
			HideScreen(navigationStack.Peek());
		}
		GameManager.SharedGameManager.LoadGameSession();
	}

	public UIWidget GetCurrentScreen()
	{
		return navigationStack.Peek();
	}

	public GUIPlayerHUD GetPlayerHUDForPlayerId(int playerId)
	{
		switch(playerId)
		{
			case 1:
				return Player1HUD;
			case 2:
				return Player2HUD;
			case 3:
				return Player3HUD;
			case 4:
				return Player4HUD;
			default:
				return Player1HUD;
		}
	}

	public void UpdateActivePlayerHUDForGameSession()
	{
		NGUITools.SetActive(Player1HUD.gameObject, false);
		NGUITools.SetActive(Player2HUD.gameObject, false);
		NGUITools.SetActive(Player3HUD.gameObject, false);
		NGUITools.SetActive(Player4HUD.gameObject, false);

		foreach (KeyValuePair<int, PlayerStats> kvp in GameManager.SharedGameManager.CurrentGameSession.ActivePlayers)
		{
			GUIPlayerHUD playerHUD = GetPlayerHUDForPlayerId(kvp.Key);

			if (kvp.Value.IsActive == true)
			{
				NGUITools.SetActive(playerHUD.gameObject, true);
			}
			else
			{
				NGUITools.SetActive(playerHUD.gameObject, false);
			}
		}
	}

	public GUIPlayerResults GetPlayerResultsForPlayerId(int playerId)
	{
		switch(playerId)
		{
		case 1:
			return Player1Results;
		case 2:
			return Player2Results;
		case 3:
			return Player3Results;
		case 4:
			return Player4Results;
		default:
			return Player1Results;
		}
	}

	public void UpdateActivePlayerResultsForGameSession()
	{
		NGUITools.SetActive(Player1Results.gameObject, false);
		NGUITools.SetActive(Player2Results.gameObject, false);
		NGUITools.SetActive(Player3Results.gameObject, false);
		NGUITools.SetActive(Player4Results.gameObject, false);
		
		foreach (KeyValuePair<int, PlayerStats> kvp in GameManager.SharedGameManager.CurrentGameSession.ActivePlayers)
		{
			GUIPlayerResults playerResults = GetPlayerResultsForPlayerId(kvp.Key);
			
			if (kvp.Value.IsActive == true)
			{
				NGUITools.SetActive(playerResults.gameObject, true);
			}
			else
			{
				NGUITools.SetActive(playerResults.gameObject, false);
			}
		}
	}

	public void StartGameTimer()
	{
		float duration = GlobalConfig.DEFAULT_GAME_DURATION;
		GameTimer.StartTimer(duration);
	}

	public bool HasTimerCompleted()
	{
		return GameTimer.TimerComplete;
	}

	public void UpdateResultsScreen()
	{
		List<PlayerStats> sortedPlayerStats = new List<PlayerStats>();

		foreach (KeyValuePair<int, PlayerStats> kvp in GameManager.SharedGameManager.CurrentGameSession.ActivePlayers)
		{
			if (kvp.Value.IsActive == true)
			{
				sortedPlayerStats.Add(kvp.Value);
			}
		}

		sortedPlayerStats.Sort( (stat1, stat2) => stat2.Score.CompareTo(stat1.Score) );

		for (int i = 0; i < sortedPlayerStats.Count; i++)
		{
			string rank = "";
			switch(i)
			{
				case 0:
					rank = "1st";
					break;
				case 1:
					rank = "2nd";
					break;
				case 2:
					rank = "3rd";
					break;
				case 3:
					rank = "4th";
					break;
			}

			GetPlayerResultsForPlayerId(sortedPlayerStats[i].PlayerId).SetResults(sortedPlayerStats[i].Score, sortedPlayerStats[i].Accolades, rank);
		}
	}
}
