﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
	public static GameManager SharedGameManager {get; set;}
	public EnvironmentManager EnvironmentManager {get; set;}
	public EntityManager EntityManager {get; set;}
	public PlayerManager PlayerManager {get; set;}
	public PowerManager PowerManager {get; set;}

	public AudioManager AudioManager {get; set;}
	public GUIManager GUIManager {get; set;}

	public GameSession CurrentGameSession {get; set;}

	void Awake()
	{
		DontDestroyOnLoad(this);

		if (SharedGameManager == null)
		{
			SharedGameManager = this;
		}

		PlayerManager = new PlayerManager();
		EnvironmentManager = gameObject.AddComponent<EnvironmentManager>();
		EntityManager = gameObject.AddComponent<EntityManager>();
		PowerManager = gameObject.AddComponent<PowerManager>();
		AudioManager = gameObject.AddComponent<AudioManager>();
		GUIManager = gameObject.AddComponent<GUIManager>();
		
		InitialiseGameSession();
	}

	// Use this for initialization
	void Start() 
	{
		AudioManager.PlayMenuBGM();
	}
	
	// Update is called once per frame
	void Update() 
	{
		// Detect end of a game session
		if (CurrentGameSession != null && CurrentGameSession.GameInProgress == true)
		{
			if (GUIManager.HasTimerCompleted() == true)
			{
				CurrentGameSession.GameInProgress = false;
				GUIManager.PushScreen(GUIManager.ResultsScreen);
				GUIManager.UpdateActivePlayerResultsForGameSession();
				GUIManager.UpdateResultsScreen();
				EnvironmentManager.DestroyEnvironment();
				EntityManager.DestroyAllCharacters();
			}
		}
	}

	public void InitialiseGameSession()
	{
		CurrentGameSession = new GameSession();
		CurrentGameSession.ConfigureForSingleplayer();
		if (GUIManager.CharacterSelectionMenu != null)
		{
			GUIManager.CharacterSelectionMenu.Reset();
		}
	}

	public void LoadGameSession()
	{
		EnvironmentManager.LoadTileMap();

		// TODO: Later there will be other stats to take into account such as which character the player is using etc.
		foreach (KeyValuePair<int, PlayerStats> kvp in CurrentGameSession.ActivePlayers)
		{
			if (kvp.Value.IsActive == true)
			{
				if (kvp.Value.IsComputer == true)
				{
					EntityManager.CreateAICharacter(kvp.Key);
				}
				else
				{
					EntityManager.CreatePlayableCharacter(kvp.Key);
				}
			}
		}

		CurrentGameSession.GameInProgress = true;

		GUIManager.StartGameTimer();
	}
}
