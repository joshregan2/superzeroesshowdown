using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EntityManager : MonoBehaviour
{
	public List<Character> AllCharacters {get; set;}

	void Awake()
	{
		AllCharacters = new List<Character>();
	}
	
	void Start()
	{
		
	}
	
	void Update()
	{
		
	}

	public void CreatePlayableCharacter(int playerId)
	{
		GameObject playableCharacterGameObject = new GameObject("Player");
		Character playableCharacter = playableCharacterGameObject.AddComponent<Character>();
		Player controlledPlayer = playableCharacterGameObject.AddComponent<Player>();


		playableCharacter.ConfigureForCharacter(CharacterConfig.CharacterTypes.TEST_CHARACTER_CIRCLE);

		GameObject dynamicShadowGameObject = new GameObject("Shadow");
		dynamicShadowGameObject.AddComponent<DynamicShadow>();
		dynamicShadowGameObject.transform.parent = playableCharacterGameObject.transform;
		dynamicShadowGameObject.transform.localPosition = new Vector3(0, -0.32f, 0);

		controlledPlayer.PlayerId = playerId;
		playableCharacter.PlayerId = playerId;

		GameManager.SharedGameManager.GUIManager.GetPlayerHUDForPlayerId(playerId).SetHealth(playableCharacter.CurrentHealth, playableCharacter.Stats.MaxHealth);
		GameManager.SharedGameManager.GUIManager.GetPlayerHUDForPlayerId(playerId).SetPower(PowersConfig.PowerTypes.EMPTY);
		GameManager.SharedGameManager.GUIManager.GetPlayerHUDForPlayerId(playerId).SetScore(0);

		Vector2 nextAvailableSpawn = GameManager.SharedGameManager.EnvironmentManager.NextAvailablePlayerSpawnLocation();
		playableCharacterGameObject.transform.position = new Vector3(nextAvailableSpawn.x, nextAvailableSpawn.y, 0);

		AllCharacters.Add(playableCharacter);
	}

	public void CreateAICharacter(int playerId)
	{
		GameObject playableCharacterGameObject = new GameObject("Player");
		Character playableCharacter = playableCharacterGameObject.AddComponent<Character>();
		Player controlledPlayer = playableCharacterGameObject.AddComponent<Player>();
		PlayerAI aiPlayer = playableCharacterGameObject.AddComponent<PlayerAI>();
		
		
		playableCharacter.ConfigureForCharacter(CharacterConfig.CharacterTypes.TEST_CHARACTER_CIRCLE);
		
		GameObject dynamicShadowGameObject = new GameObject("Shadow");
		dynamicShadowGameObject.AddComponent<DynamicShadow>();
		dynamicShadowGameObject.transform.parent = playableCharacterGameObject.transform;
		dynamicShadowGameObject.transform.localPosition = new Vector3(0, -0.32f, 0);
		
		controlledPlayer.PlayerId = playerId;
		playableCharacter.PlayerId = playerId;
		
		GameManager.SharedGameManager.GUIManager.GetPlayerHUDForPlayerId(playerId).SetHealth(playableCharacter.CurrentHealth, playableCharacter.Stats.MaxHealth);
		GameManager.SharedGameManager.GUIManager.GetPlayerHUDForPlayerId(playerId).SetPower(PowersConfig.PowerTypes.EMPTY);
		GameManager.SharedGameManager.GUIManager.GetPlayerHUDForPlayerId(playerId).SetScore(0);
		
		Vector2 nextAvailableSpawn = GameManager.SharedGameManager.EnvironmentManager.NextAvailablePlayerSpawnLocation();
		playableCharacterGameObject.transform.position = new Vector3(nextAvailableSpawn.x, nextAvailableSpawn.y, 0);
		
		AllCharacters.Add(playableCharacter);
	}

	public void DestroyAllCharacters()
	{
		for (int i = 0; i < AllCharacters.Count; i++)
		{
			GameObject.Destroy(AllCharacters[i].gameObject);
		}
		AllCharacters = new List<Character>();
	}
}

