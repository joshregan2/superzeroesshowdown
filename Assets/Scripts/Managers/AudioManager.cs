﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour 
{
	private AudioClip menuAudioClip;

	public AudioSource AudioSourceBGM {get; set;}

	void Awake() 
	{
		menuAudioClip = Resources.Load<AudioClip>("Audio/SZS Dust Off Your Cape Loop");
		AudioSourceBGM = gameObject.AddComponent<AudioSource>();
		AudioSourceBGM.bypassEffects = true;
		AudioSourceBGM.bypassListenerEffects = true;
		AudioSourceBGM.bypassReverbZones = true;
	}

	// Use this for initialization
	void Start() 
	{

	}
	
	// Update is called once per frame
	void Update() 
	{
	
	}

	public void PlayMenuBGM()
	{
		AudioSourceBGM.Stop();

		AudioSourceBGM.clip = menuAudioClip;
		AudioSourceBGM.loop = true;
		AudioSourceBGM.Play();
	}
}
