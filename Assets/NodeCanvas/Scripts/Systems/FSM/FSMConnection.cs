﻿using UnityEngine;

namespace NodeCanvas.FSM{

	//Identical to conditional connection for now, but better to be on the safe side for the future ;-)
	[AddComponentMenu("")]
	public class FSMConnection : ConditionalConnection {

	}
}