using UnityEditor;
using NodeCanvas.FSM;

namespace NodeCanvasEditor{

	[CustomEditor(typeof(FSMContainer))]
	public class FSMContainerInspector : NodeGraphContainerInspector{

	}
}