using UnityEngine;
using System.Collections;

namespace NodeCanvas.DialogueTree{

	public enum DLGEvents{
		
		OnActorSpeaking,
		OnDialogueOptions,
		OnDialogueStarted,
		OnDialogueFinished
	}
}