using UnityEditor;
using NodeCanvas.BehaviourTree;

namespace NodeCanvasEditor{

	[CustomEditor(typeof(BTContainer))]
	public class BTContainerInspector : NodeGraphContainerInspector{

	}
}