﻿using UnityEngine;
using System.Collections;

namespace NodeCanvas.Actions{

	[ScriptCategory("GameObject")]
	[AgentType(typeof(Transform))]
	public class AddComponent : ActionTask {

		[RequiredField]
		public string componentName;

		protected override string actionInfo{
			get {return "Add '" + componentName + "' Component";}
		}

		protected override void OnExecute(){

			if (agent.GetComponent(componentName) == null)
				agent.gameObject.AddComponent(componentName);

			EndAction();
		}
	}
}